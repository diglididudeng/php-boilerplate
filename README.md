# [PHP Boilerplate](https://github.com/DiglidiDudeNG/PHP-Boilerplate)

PHP Boilerplate is a boilerplate meant for generic PHP projects that use PDO/MySQL requests through the local database.
It accelerates the start-up of a new project in PHP and removes most of the hassle from the usual routine that I do when I make new projects in PHP.

### Prerequisites

You'll need the following in order to fully enjoy this boilerplate :

- A local or web server. (Apache recommended)
- A compatible version of PHP. (5.6 recommended)
- A compatible version of MySQL.
- Common sense (Very important!)

Of course, you can always go straight ahead and enjoy it.

### Great, what's next?

I'm not too sure as of right now. I'll edit this part later !