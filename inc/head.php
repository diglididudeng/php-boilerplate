<?php
/**
 * HEAD.php
 *
 * Contains the very beginning of your typical, visual web page.
 * Also contains some helpful pre-header variables, function calls and other thingamajigs.
 */

// Calls the database variables and functions which you absolutely need.
require_once($rootPath."db/db_connect.php");
require_once($rootPath."db/db_functions.php");

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="<?=$rootPath?>css/normalize.css">
	<link rel="stylesheet" href="<?=$rootPath?>css/main.css">
	<script src="<?=$rootPath?>js/vendor/modernizr-2.6.2.min.js"></script>
</head>

<?php

?>