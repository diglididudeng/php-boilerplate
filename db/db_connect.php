<?php
/**
 * DB_CONNECT.php
 *
 * Instantiates the required database informations, such as host,
 * username, password and database name.
 */

// Address of your host.
define("PDO_HOST", "localhost");

// Username to use to access the database.
define("PDO_USER", "root");

// Password for that user.
define("PDO_PASS", "");

// Name of the database where the
define("PDO_DB",   "test");